# README #

### Todo MEAN stack app ###

* This app is aiming to improve my Angular 1.x skills. 
* v. 0.9

### How do I get set up? ###

* Download and install node.js (https://nodejs.org/en/download/) 
* Download and install latest MongoDB (https://www.mongodb.com/download-center#community)
* Run "npm install" in the root directory of the project
* Run "node server.js" in the root directory of the project

### Application capabilities ###

* User can add his tasks in first planning stage
* User can delete those task, which has to be canceled or re-scheduled
* User can edit tasks
* User can log in with credentials
* Editing only permitted for user`s tasks