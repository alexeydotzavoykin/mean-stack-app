var mongoose = require('mongoose');

	module.exports = mongoose.model('User',{
		id: String, //users id, will be used to differ the todos
		username: String,
		password: String, //credentials for user
		email: String,
		firstName: String,
		lastName: String //personal data
	});