var mongoose = require('mongoose');

    module.exports = mongoose.model('Todo', {
        text : String,  // !not null
        about : String, //full description of what is expected
        budget : Number, // Amount of cash, that would be used to deal with task !not null
        onCard : Boolean, //Displays if money will be taken from card
        importance : Number, //Importance of task on scale ! from 0 to 10
        address : String, //address where task should be completed
        time : String, //date when task should be completed !number of hours
        done : Boolean, //Is completed task?
        user_id : String
    });