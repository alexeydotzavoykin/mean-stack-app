// app/routes.js

var Todo = require('./models/todo');
var express = require('express');
var router = express.Router();

/* var isAuthenticated = function (req, res, next) {
    if (req.isAuthenticated())
        next();
    else
        res.redirect('/login');
}; */
module.exports = function(passport) {

    router.get('/api/todos', function(req, res) {
       // isAuthenticated(req, res, function () {
        if (req.isAuthenticated()) {
            Todo.find(function (err, todos) {
                if (err)
                    res.send(err)
                let object = {};
                object.username = req.user._doc.username;
                object.todos = todos;
                res.json(object);
            });
        }
        else res.send(err);
    });

    //get one todo
    router.get('/api/todos/:todo_id', function(req, res) {
       // isAuthenticated(req, res, function () {
        if (req.isAuthenticated()) {
            req.param('todo_id');
            Todo.find({
                _id: req.params.todo_id
            }, function (err, todo) {
                if (err)
                    res.send(err);
                else
                    res.json(todo);
            });
        }
        else res.send(err);
        //})
    });

    router.post('/api/todos', function(req, res) {
        if (req.isAuthenticated()){
            Todo.create({
                text: req.body.text,
                importance: req.body.importance,
                about: req.body.about,
                budget: req.body.budget,
                onCard: req.body.onCard,
                address: req.body.address,
                time: req.body.time,
                done: false,
                user_id: req.user._doc.username
            }, function (err, todo) {
                if (err)
                    res.send(err);
                Todo.find(function (err, todos) {
                    if (err)
                        res.send(err)
                    res.json(todos);
                });
            });
        }
        else
            res.send(err)
        //})
    });

    router.put('/api/todos/:todo_id', function(req, res) {
        //isAuthenticated(req, res, function () {
        if (req.isAuthenticated()){
            var todoVer = {
                id: req.params.todo_id,
                text: req.body.text,
                importance: req.body.importance,
                about: req.body.about,
                budget: req.body.budget,
                onCard: req.body.onCard,
                address: req.body.address,
                time: req.body.time,
                done: false,
                user_id: req.user._doc.username
            };
            Todo.findOneAndUpdate(
                {_id: todoVer.id}, todoVer, function (err, todos) {
                    if (err)
                        res.send(err);
                    Todo.find(function (err, todos) {
                        if (err)
                            res.send(err)
                        res.json(todos);
                    });
                });
       // })
        }
        else
            res.send(err)
    });

    // delete a todo
    router.delete('/api/todos/:todo_id', function(req, res) {
        //isAuthenticated(req, res, function () {
        if (req.isAuthenticated()) {
            Todo.remove({
                _id: req.params.todo_id
            }, function (err, todo) {
                if (err)
                    res.send(err);

                Todo.find(function (err, todos) {
                    if (err)
                        res.send(err)
                    res.json(todos);
                });
            });
            //})
        }
        else
            res.send(err);
    });

    router.get('/home', function(req, res){
        if (req.isAuthenticated()){
            //res.user_id = req.user._doc.username;
            res.sendfile('./public/home.html');
        }
        else
            res.redirect('/login');
        //isAuthenticated(req, res, res.sendfile('./public/home.html'));
    });

    router.get('/login', function(req, res) {
        if (req.isAuthenticated())
            res.redirect('/home');
        else
            res.sendfile('./public/enter.html');
    });

    router.get('/', function(req, res) {
        res.redirect('/home');
    });

    router.post('/login', passport.authenticate('login', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash : true
    }));

    /* Handle Registration POST */
    router.post('/register', passport.authenticate('register', {
        successRedirect: '/login',
        failureRedirect: '/register',
        failureFlash : true
    }));

    router.get('/register', function(req, res) {
        if (req.isAuthenticated())
            res.redirect('/home');
        else
            res.sendfile('./public/register.html');
    });

    router.get('/signout', function(req, res) {
        req.logout();
        res.redirect('/home');
    });

    return router;
};