angular.module('todoService', [])
    .factory('Todos', function($http) {
        return {
            get : function() {
                return $http.get('/api/todos');
            },
            getone : function(id) {
                return $http.get('/api/todos/' + id);
            },
            create : function(todoData) {
                return $http.post('/api/todos', todoData);
            },
            delete : function(id) {
                return $http.delete('/api/todos/' + id);
            },
            update : function (todoData) {
                return $http.put('/api/todos/' + todoData._id, todoData);
            }
        }
    });