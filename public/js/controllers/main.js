angular.module('todoController', [])

    .controller('mainController', function($scope, $http, $window, Todos) {

        $scope.items = ['todoPage', 'addingTodoPage', 'editingTodoPage', 'todoIsEditing'];
        $scope.selection = $scope.items[0];
        $scope.formData = {};
        $scope.credForm = {};
        $scope.id = {};
        $scope.todo = {};
        $scope.cred = {};

        $scope.username = "0";

        let creatingTodo = function(answerForm) {
            console.log($scope.todo);
            if (!$.isEmptyObject($scope.todo)) {
                if (answerForm.$valid) {
                    Todos.create($scope.todo)
                        .success(function (data) {
                            $scope.todo = {};
                            $scope.selection = $scope.items[0];
                            $scope.todos = data.filter(function(obj) {
                                return obj.user_id === $scope.username;
                            });
                        });
                }
            }
            console.log($scope.selection);
        };

        let deletingTodo = function(id) {
            Todos.delete(id)
                .success(function(data) {
                    $scope.todo = {};
                    $scope.todos = data.filter(function(obj) {
                        return obj.user_id === $scope.username;
                    });
                    $scope.selection = $scope.items[0];
                });

        };

        let updatingTodo = function () {
            $scope.todo._id = $scope.id;
            Todos.update($scope.todo)
                .success(function(data) {
                    $scope.todos = data.filter(function( obj ) {
                        return obj.user_id === $scope.username;
                    });
                });
            $scope.selection = $scope.items[2];
        };

        let editingPageOpened = function () {
            $scope.selection = $scope.items[3];
            console.log($scope.todo);
        };

        let openingTodo = function(id) {
            $scope.selection = $scope.items[2];
            Todos.getone(id)
                .success(function(data) {
                    $scope.todo = data[0];
                    $scope.id = id;
                });
        };

        let addingCancellation = function () {
            $scope.selection = $scope.items[0];
            $scope.todos = ($scope.todos).filter(function( obj ) {
                return obj.user_id === $scope.username;
            });
        };

        let addingPageOpened = function () {
            $scope.selection = $scope.items[1];
            $scope.todo = {};
        };

        // GET =====================================================================
       Todos.get()
            .success(function(data) {
                $scope.username = data.username;
                $scope.todos = data.todos;
                $scope.todos = $scope.todos.filter(function( obj ) {
                    return obj.user_id === $scope.username;
                });
            });

        // CREATE ==================================================================
        $scope.createTodo = creatingTodo;

        // DELETE ==================================================================
        $scope.deleteTodo = deletingTodo;

        $scope.updateChangedTodo = updatingTodo;

        $scope.editingStart = editingPageOpened;

        $scope.openTodo = openingTodo;

        $scope.addingTodoPageClick = addingPageOpened;

        $scope.cancelAdd = addingCancellation;

    });